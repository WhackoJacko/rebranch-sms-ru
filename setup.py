from os.path import join, dirname

from setuptools import setup


setup(
    name='rebranch_sms_ru',
    version=u'0.3.1',
    dependency_links=[
        u'https://bitbucket.org/Franz_ru/rebranch-shortcuts#egg=rebranch_shortcuts-0.1.0',
    ],
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    author='',
    url='https://bitbucket.org/Franz_ru/rebranch-sms-ru',
)