# -*- coding: utf8 -*-
import re

from rebranch_shortcuts.utils import safe_cast
from rebranch_sms_ru.utils import clean_phone


class CeleryMessageMixin(object):
    QUEUE_TYPE_PERIODIC = 0
    QUEUE_TYPE_REGULAR = 1

    QUEUE_TYPE_CHOICES = (
        (QUEUE_TYPE_PERIODIC, u'Периодический'),
        (QUEUE_TYPE_REGULAR, u'Обычный'),
    )

    def send_async(self):
        raise NotImplementedError


class BaseMessageMixin(object):
    def apply_context(self, context):
        for search, replace in context.iteritems():
            replace = safe_cast(replace, unicode)
            self.content = self.content.replace(search, replace)

    @staticmethod
    def clean_phone(phone):
        return clean_phone(phone)