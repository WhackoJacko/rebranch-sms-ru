import datetime
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, Text, DateTime, ForeignKey
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker, scoped_session

from rebranch_sms_ru.config import sms_settings as config
from rebranch_sms_ru.mixins import CeleryMessageMixin, BaseMessageMixin

database_url = URL(drivername=config.DATABASE[u'ENGINE'], username=config.DATABASE[u'USER'],
                   password=config.DATABASE[u'PASSWORD'], host=config.DATABASE[u'HOST'],
                   database=config.DATABASE[u'NAME'])

engine = create_engine(database_url, echo=True)

Base = declarative_base()

db = scoped_session(sessionmaker(bind=engine))


class ContentType(Base):
    __tablename__ = 'django_content_type'

    id = Column(Integer, primary_key=True)

    name = Column(String(length=100))
    app_label = Column(String(length=100))
    model = Column(String(length=100))


class Message(Base, BaseMessageMixin, CeleryMessageMixin):
    __tablename__ = 'sms_ru_message'

    id = Column(Integer, primary_key=True)
    content_type_id = Column(Integer, ForeignKey(ContentType.id))
    object_id = Column(Integer)

    recipient = Column(String(length=15))
    content = Column(Text())
    api_id = Column(String(length=255))
    sent = Column(DateTime)
    queue_type = Column(Integer)
    created = Column(DateTime, default=datetime.datetime.now)

    content_type = relationship(ContentType)

    def __init__(self, recipient, content, queue_type, sent=None, api_id=None):
        super(Base, self).__init__()
        self.recipient = recipient
        self.content = content
        self.api_id = api_id
        self.sent = sent
        self.queue_type = queue_type

    def save(self):
        db.add(self)
        db.commit()

    @classmethod
    def filter(cls, **kwargs):
        return db.query(cls).filter_by(**kwargs)

    @classmethod
    def get(cls, **kwargs):
        return cls.filter(**kwargs).first()

    def send_async(self):
        from rebranch_sms_ru.tasks import send_sqlalchemy_message

        send_sqlalchemy_message.apply_async(kwargs={u'message_id': self.id}, queue=u'sms_ru.sqlalchemy')