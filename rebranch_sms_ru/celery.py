from __future__ import absolute_import

from celery import Celery

from .config import settings

app = Celery(backend=settings.CELERY_RESULT_BACKEND, broker=settings.BROKER_URL, queues=[u'sms_ru'])