# -*- coding: utf8 -*-
import urllib
import urlparse
import urllib2
import re

from .statuses import STATUS_CHOICES


class SMSSender(object):
    _PARAM_API_ID = u'api_id'
    _PARAM_RECIPIENT = u'to'
    _PARAM_CONTENT = u'text'

    _API_METHOD_SEND = u'/sms/send'

    class DictBasedValueDescriptor(object):
        def __init__(self, name):
            self.name = name

        def __get__(self, instance, owner):
            return instance._params.get(self.name)

        def __set__(self, instance, value):
            if isinstance(value, unicode):
                value = value.encode(u'utf8')
            instance._params[self.name] = value

    def _flush_params(self):
        new_params = {
            self._PARAM_API_ID: self._api_id
        }
        self._params = new_params

    def _get_params(self):
        params = self._params.copy()
        return params

    @property
    def params(self):
        params = self._params.copy()
        return params

    _content = DictBasedValueDescriptor(_PARAM_CONTENT)
    _api_id = DictBasedValueDescriptor(_PARAM_API_ID)
    _recipient = DictBasedValueDescriptor(_PARAM_RECIPIENT)

    def _call_api_method(self, method):
        url = urlparse.urljoin(self._api_url, method)
        url_params = urllib.urlencode(self._get_params())
        full_url = u'%s?%s' % (url, url_params)
        request = urllib2.Request(url=full_url, )
        opener = urllib2.build_opener(urllib2.HTTPHandler(debuglevel=int(self._debug_mode)))
        try:
            response = opener.open(request)
            res = response.read()
        except:
            return {
                u'status': u'-1',
                u'sms_id': u'None',
                u'balance': u'None'
            }
        self._flush_params()
        status, sms_id, balance = (res.split() + [None, None])[:3]
        res = {
            u'status': status,
            u'sms_id': sms_id,
            u'balance': balance
        }
        return res

    def __init__(self, api_id, api_url=u'http://sms.ru', debug=False):
        super(SMSSender, self).__init__()
        self._params = {}
        self._debug_mode = debug
        self._api_url = api_url
        self._api_id = api_id

    @staticmethod
    def get_status_description(status):
        return STATUS_CHOICES.get(status, STATUS_CHOICES[u'default'])

    def send(self, recipient, content):
        self._recipient = recipient
        self._content = content
        api_response = self._call_api_method(self._API_METHOD_SEND)
        api_response[u'status_description'] = self.get_status_description(api_response[u'status'])
        return api_response


def clean_phone(phone):
    return re.sub(u'\D', u'', phone)[-10:]