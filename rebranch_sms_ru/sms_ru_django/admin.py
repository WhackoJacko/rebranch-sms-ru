# -*- coding:utf-8 -*-
from django.contrib import admin

from rebranch_shortcuts.django.admin import ModelAdminWithFKLink
from rebranch_sms_ru.sms_ru_django.models import Message


class MessageModelAdmin(ModelAdminWithFKLink):
    def link_to_content_object(self, instance):
        return self.__getattr__('link_to_content_object')(instance)

    link_to_content_object.allow_tags = True
    link_to_content_object.short_description = u'Связанный объект'

    def is_sent(self, instance):
        if instance.sent:
            return instance.sent
        else:
            return u'В очереди'

    is_sent.short_description = u'Отправлено'

    list_display = (u'recipient', u'created', u'is_sent', u'queue_type', u'link_to_content_object')


admin.site.register(Message, MessageModelAdmin)