try:
    from django.conf import settings
except ImportError:
    settings = None
if not settings:
    try:
        from flask_configurations.configuration import settings
    except:
        settings = None
if not settings:
    try:
        import settings
    except ImportError:
        settings = None


assert settings is not None, u'No valid settings module found'


class SMSConfig(object):
    def __getattribute__(self, item):
        return getattr(settings, item)


sms_settings = SMSConfig()