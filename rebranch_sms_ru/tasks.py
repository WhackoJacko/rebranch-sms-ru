# -*- coding:utf-8 -*-
import datetime

from rebranch_sms_ru.config import sms_settings as config
from .utils import SMSSender
from .celery import app as celery_app

_ERROR_TEMPLATE = u'CMC-cообщение с id "{0}" не было отправлено, сервис рассылки вернул вернул код {1} ("{2}")'


@celery_app.task(ignore_result=True, name=u'')
def send_sqlalchemy_message(message_id):
    from rebranch_sms_ru.sqlalchemy.models import Message

    message = Message.get(id=message_id)
    send_message(message)


@celery_app.task(ignore_results=True, name=u'rebranch_sms_ru.tasks.send_django_message')
def send_django_message(message_id):
    from rebranch_sms_ru.sms_ru_django.models import Message

    message = Message.objects.get(id=message_id)
    send_message(message)


def send_message(message):
    client = SMSSender(api_id=config.SMS_ID)
    api_response = client.send(message.recipient, message.content)
    if api_response[u'status'] == u'100':
        api_response[u'status'] = u'success'
        message.sent = datetime.datetime.now()
        message.api_id = api_response[u'sms_id']
        message.save()
    else:
        api_response[u'error'] = _ERROR_TEMPLATE.format(
            message.id, api_response[u'status'],
            api_response[u'status_description'],
        )
        api_response[u'status'] = u'fail'
    return api_response